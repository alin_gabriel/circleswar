package
{
	import flash.display.*;
	import flash.geom.Point;
	
	public class Player extends Actor
	{
		
		public function Player()
		{
			var dx:Number;
			maxSpeed = 7;
			attackRange = 80;
			skin = new Sprite();
			skin.graphics.beginFill(0xff0000);
			skin.graphics.drawCircle(0,0,10);
			skin.graphics.endFill();
			drawShootingArea(skin,315,45,attackRange,0,0,1);
		}
		
		public function move(x:Number,y:Number,dir:Number):void
		{
			position(skin.x + x,skin.y + y,dir);
		}
		
		
		public function isInEnemyViewArea(enemy:Enemy):int
		{
			var dx:Number = skin.x - enemy.skin.x;
			var dy:Number = skin.y - enemy.skin.y;
			var distance:Number = Math.sqrt(dx * dx + dy * dy);
			var angle:Number = Math.atan(dy / dx) * (180 / Math.PI);
			if ( dx < 0 )
			{
				if ( dy < 0 )
					angle -= 180;
				else
					angle += 180;
			}
			var diffAngle:Number = differenceOfAngles(enemy.skin.rotation,angle);
			
			if ( distance <= enemy.viewRange + 10 && Math.abs(diffAngle) <= 45 )
				return 1;
			return 0;
		}
		
		public function isInEnemyShootingArea(enemy:Enemy):int
		{
			var dx:Number = skin.x - enemy.skin.x;
			var dy:Number = skin.y - enemy.skin.y;
			var distance:Number = Math.sqrt(dx * dx + dy * dy);
			var angle:Number = Math.atan(dy / dx) * (180 / Math.PI);
			if ( dx < 0 )
			{
				if ( dy < 0 )
					angle -= 180;
				else
					angle += 180;
			}
			var diffAngle:Number = differenceOfAngles(enemy.skin.rotation,angle);
			
			if ( distance <= enemy.attackRange + 10 && Math.abs(diffAngle) <= 45 )
				return 1;
			return 0;
		}
	}
}