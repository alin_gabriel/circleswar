package
{
	import flash.display.*;
	import flash.geom.*;
		
	public class Enemy extends Actor
	{
		public var pointToGo:Point;
		public var viewRange:int;
		public function Enemy()
		{
			maxSpeed = 3;
			attackRange = 40;
			viewRange = 100;
			pointToGo = new Point(Math.random() * 600 + 200, Math.random() * 250 + 250);
			skin = new Sprite();
			skin.graphics.beginFill(0x0000ff);
			skin.graphics.drawCircle(0,0,10);
			skin.graphics.endFill();
			drawShootingArea(skin,315,45,attackRange,skin.x,skin.y,2);
			drawShootingArea(skin,315,45,viewRange,skin.x,skin.y,2);
		}
		
		public function move(x:Number,y:Number,dir:Number):void
		{
			position(skin.x + x,skin.y + y,dir);
		}
		
		public function isInPlayerShootingArea(player:Player):int
		{
			var dx:Number = skin.x - player.skin.x;
			var dy:Number = skin.y - player.skin.y;
			var distance:Number = Math.sqrt(dx * dx + dy * dy);
			var angle:Number = Math.atan(dy / dx) * (180 / Math.PI);
			if ( dx < 0 )
			{
				if ( dy < 0 )
					angle -= 180;
				else
					angle += 180;
			}
			var diffAngle:Number = differenceOfAngles(player.skin.rotation,angle);
			
			if ( distance <= player.attackRange + 10 && Math.abs(diffAngle) <= 45 )
				return 1;
			return 0;
		}
	}
}