package
{
	import flash.display.Sprite;
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;

	public class StateManager
	{
		private var states:Dictionary = new Dictionary();
		private var currentState:State;
		public var dispatcher:IEventDispatcher
		
		public function StateManager()
		{
		}
		
		public function addState(state:State, name:String):void
		{
			states[name] = state;
			state.dispatcher = dispatcher;
			state.initialize();
		}
		
		public function changeState(name:String):void
		{
			var newState:State = states[name];
			if (!newState) {
				throw new Error("invalid state");
			}
			if (newState == currentState) return;
			if (currentState) {
				currentState.exitState();
			}
			
			currentState = newState;
			newState.enterState();
			
		}
	}
}