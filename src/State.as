package
{
	import flash.display.*;
	import flash.events.*;
	
	public class State
	{
		public var running:Boolean = false;
		
		public var dispatcher:IEventDispatcher
		
		public function State()
		{
		}
		
		public function exitState():void 
		{
			running = false;
		}
		
		public function enterState():void 
		{
			running = true;
		}
		
		public function initialize():void 
		{
			
		}
	}
}