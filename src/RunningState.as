package
{	
	import events.GameEvent;
	
	import flash.display.*;
	import flash.events.*;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	
	public class RunningState extends State
	{
		
		public static const NAME:String = "RunningState";
		
		private var me:Player;
		private var enemies:Array;
		public var oldMaxSpeed:int;
		public var distance:Number = 0;
		private var dx:int;
		private var dy:int;
		private var container:Sprite;
		private var score:int;
		private var numberOfEnemies:int
		private var textField:TextField;
		
		
		public function RunningState(containerPlayer:Sprite)
		{
			score = 0;
			numberOfEnemies = 3;
			this.container = containerPlayer;
			textField = new TextField();
			textField.defaultTextFormat = new TextFormat("Arial",20,"black",true);
			textField.text = "Score : " + score;
			container.stage.addChild(textField);
			me = new Player();
			me.position(0,0,0);
			oldMaxSpeed = me.maxSpeed;
			enemies = new Array();
			for (var i:int = 0; i < numberOfEnemies; i++) 
			{
				enemies[i] = new Enemy();
				enemies[i].position(Math.random() * 600 + 200,Math.random() * 250 + 200,0);
				container.stage.addChildAt(enemies[i].skin,1);
			}
			containerPlayer.addChild(me.skin);
		}
		
		override public function enterState():void {
			super.enterState();
			score = 0;
			textField.text = "Score : " + score;
			numberOfEnemies = 3;
			for (var i:int = 0; i < numberOfEnemies; i++) 
			{
				enemies[i].position(Math.random() * 800,Math.random() * 450,0);
				while ( me.isInEnemyViewArea(enemies[i]) == 1 )
				{
					enemies[i].position(Math.random() * 800,Math.random() * 450,0);
				}
			}
			while (enemies.length > 3) {
				container.stage.removeChild(enemies.pop().skin);
			}
			
			container.stage.addEventListener(Event.ENTER_FRAME,onEnterFrame);
			container.stage.addEventListener(KeyboardEvent.KEY_DOWN,onSpacePressed);
			container.stage.addEventListener(KeyboardEvent.KEY_DOWN,onAPressed);
		}
		
		override public function exitState():void {
			super.exitState();
			container.stage.removeEventListener(Event.ENTER_FRAME,onEnterFrame);
			container.stage.removeEventListener(KeyboardEvent.KEY_DOWN,onSpacePressed);
			container.stage.removeEventListener(KeyboardEvent.KEY_DOWN,onAPressed);
		}
		
		protected function onEnterFrame(event:Event):void
		{
			if (!running) return;
			
			if (me.maxSpeed == oldMaxSpeed) {
				dx = container.mouseX - me.skin.x;
				dy = container.mouseY - me.skin.y;
			}
			
			var oldX:int = me.skin.x;
			var oldY:int = me.skin.y;
			var length:Number = Math.sqrt(dx * dx + dy * dy);
			var ratio:Number = length / me.maxSpeed;
			var angle:Number;
			if ( ratio > 0.5 )
			{
				angle = Math.atan(dy / dx) * (180 / Math.PI);
				if ( dx < 0 )
				{
					if ( dy < 0 )
						angle -= 180;
					else
						angle += 180;
				}
				me.move(dx / ratio,dy / ratio,angle);
				if (me.maxSpeed != oldMaxSpeed) {
					distance += Math.sqrt((me.skin.x - oldX) * (me.skin.x - oldX) + (me.skin.y - oldY) * (me.skin.y - oldY));
					if ( distance > 200 ) {
						me.maxSpeed = oldMaxSpeed;
						distance = 0;
					}
				}
			}
			var dxe:Number, dye:Number
			for (var i:int = 0; i < enemies.length; i++) 
			{
				if ( me.isInEnemyViewArea(enemies[i]) == 1 )
				{
					enemies[i].pointToGo.x = me.skin.x;
					enemies[i].pointToGo.y = me.skin.y;
				}
				if ( me.isInEnemyShootingArea(enemies[i]) == 1 )
				{
					dispatcher.dispatchEvent(new GameEvent(GameEvent.PLAYER_DEATH));
				}
				dxe = enemies[i].pointToGo.x - enemies[i].skin.x;
				dye = enemies[i].pointToGo.y - enemies[i].skin.y;
				length = Math.sqrt(dxe * dxe + dye * dye);
				ratio = length / enemies[i].maxSpeed;
				if ( ratio > 0.5 )
				{
					angle = Math.atan(dye / dxe) * (180 / Math.PI);
					if ( dxe < 0 )
					{
						if ( dye < 0 )
							angle -= 180;
						else
							angle += 180;
					}
					enemies[i].move(dxe / ratio, dye / ratio, angle);
				}
				else
				{
					enemies[i].pointToGo.x = Math.random() * 800;
					enemies[i].pointToGo.y = Math.random() * 450;
				}
			}
		}
		
		protected function onSpacePressed(ke:KeyboardEvent):void {
			if (!running) return;
			distance = 0;
			if ( ke.charCode == Keyboard.SPACE && oldMaxSpeed == me.maxSpeed )
				me.maxSpeed *= 4;
		}
		
		protected function onAPressed(ke:KeyboardEvent):void
		{
			if (!running) return;
			if ( ke.keyCode == 65 )
			{
				for (var i:int = 0; i < numberOfEnemies; i++) 
				{
					if ( enemies[i].isInPlayerShootingArea(me) == 1 )
					{
						enemies[i].skin.x = Math.random() * 800;
						enemies[i].skin.y = Math.random() * 450;
						score++;
						textField.text = "Score : " + score;
						if ( score % 10 == 0 )
						{
							numberOfEnemies++;
							var enemy:Enemy = new Enemy();
							enemies.push(enemy);
							enemies[enemies.length - 1].position(Math.random() * 800,Math.random() * 450,0);
							while ( me.isInEnemyShootingArea(enemies[i]) == 1 )
							{
								enemies[enemies.length - 1].position(Math.random() * 800,Math.random() * 450,0);
							}
							container.stage.addChildAt(enemies[enemies.length - 1].skin,1);
						}
					}
				}
			}
		}
		
	}
}