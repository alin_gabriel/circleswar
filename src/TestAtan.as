package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	[SWF(width="800", height="450", backgroundColor="#ffffff", frameRate="30")]
	
	public class TestAtan extends Sprite
	{
		public var a:Sprite;
		private var point:Point;
		public function TestAtan()
		{
			super();
			
			addEventListener(Event.ENTER_FRAME,onEF);
			point = new Point(stage.stageWidth/2,stage.stageHeight/2);
			
			trace(point.x,point.y);
			a = new Sprite();
			a.graphics.beginFill(0x0);
			a.graphics.drawCircle(point.x,point.y,10);
			a.graphics.endFill();
			stage.addChild(a);
		}
		
		protected function onEF(e:Event):void
		{
			var dx:int = mouseX - point.x;
			var dy:int = mouseY - point.y;
			var angle:Number = Math.atan(dy / dx) * (180 / Math.PI);
			if ( dx < 0 )
			{
				if ( dy < 0 )
					angle -= 180;
				else
					angle += 180;
			}
			if ( dy < 0 )
				angle = 360 + angle;
			trace(angle);
		}
	}
}