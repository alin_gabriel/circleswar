package
{
	import flash.display.*;

	public class Actor
	{
		public var attackRange:int;
		public var maxSpeed:int;
		public var skin:Sprite;
		public function Actor()
		{
		}
		
		public function position(x:Number, y:Number, dir:Number):void
		{
			skin.x = x;
			skin.y = y;
			skin.rotation = dir;
		}
		
		public function drawShootingArea(holder:Sprite, startAngle:Number, endAngle:Number, segmentRadius:Number, xpos:Number, ypos:Number, step:Number):void 
		{
			holder.graphics.lineStyle(2, 0x0);
			
			var originalEnd:Number = -1;
			if(startAngle > endAngle)
			{
				originalEnd = endAngle;
				endAngle = 360;
			}
			var degreesPerRadian:Number = Math.PI / 180;
			var theta:Number;
			startAngle *= degreesPerRadian;
			endAngle *= degreesPerRadian;
			step *= degreesPerRadian;
			
			holder.graphics.moveTo(xpos, ypos);
			for (theta = startAngle; theta < endAngle; theta += Math.min(step, endAngle - theta))
				holder.graphics.lineTo(xpos + segmentRadius * Math.cos(theta), ypos + segmentRadius * Math.sin(theta));
			holder.graphics.lineTo(xpos + segmentRadius * Math.cos(endAngle), ypos + segmentRadius * Math.sin(endAngle));
			
			if(originalEnd > -1)
			{
				startAngle = 0;
				endAngle = originalEnd * degreesPerRadian;
				for (theta = startAngle; theta < endAngle; theta += Math.min(step, endAngle - theta)) 
					holder.graphics.lineTo(xpos + segmentRadius * Math.cos(theta), ypos + segmentRadius * Math.sin(theta));
				holder.graphics.lineTo(xpos + segmentRadius * Math.cos(endAngle), ypos + segmentRadius * Math.sin(endAngle));
			}
			holder.graphics.lineTo(xpos, ypos);
		}
		
		protected function differenceOfAngles(startAngle:Number, endAngle:Number):Number
		{
			if ( (startAngle >=0 && endAngle >= 0) || (startAngle <= 0 && endAngle <= 0) )
				return endAngle - startAngle;
			if ( (startAngle >= 0 && endAngle <= 0) || (startAngle <= 0 && endAngle >= 0) )
				return Math.max(endAngle + startAngle, endAngle - startAngle);
			return 0;
		}
	}
}