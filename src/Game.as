package
{
	import events.GameEvent;
	
	import flash.display.*;
	import flash.events.*;
	import flash.ui.Keyboard;
	
	[SWF(width="800", height="450", backgroundColor="#ffffff", frameRate="30")]
	
	public class Game extends Sprite
	{
		private var containerPlayer:Sprite;
		private var containerEnemies:DisplayObjectContainer;
		private var stateManager:StateManager;

		private var asdf:int;
		public function Game() {
			
			asdf = 1;
			stage.align = StageAlign.BOTTOM_LEFT;
			containerPlayer = new Sprite();
			stage.addChild(containerPlayer);
			stateManager = new StateManager();
			stateManager.dispatcher = this;
			
			stateManager.addState(new RunningState(containerPlayer),RunningState.NAME);
			stateManager.addState(new EndState(containerPlayer),EndState.NAME);
			
			addEventListener(GameEvent.PLAYER_DEATH,onPlayerDeath);
			addEventListener(GameEvent.RESTART,onRestart);
			stateManager.changeState(RunningState.NAME);
		}
		
		protected function onRestart(event:Event):void
		{
			stateManager.changeState(RunningState.NAME);
			stage.focus = stage;
		}
		
		protected function onPlayerDeath(event:Event):void {
			stateManager.changeState(EndState.NAME);
		}
	}
}