package
{
	import events.GameEvent;
	
	import flash.display.*;
	import flash.events.*;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class EndState extends State
	{
		public static const NAME:String = "EndState";
		private var textField:TextField;
		private var container:Sprite;

		private var button:Sprite;
		
		public function EndState(container:Sprite)
		{
			this.container = container;
			
			button = new Sprite();
			
			textField = new TextField();
			textField.defaultTextFormat = new TextFormat("Arial",30,"0x00ff00",true);
			textField.text = "RESTART";
			//textField.setTextFormat(new TextFormat("Arial",30,0xFF0000));
			textField.x = container.stage.stageWidth / 2 - 100;
			textField.y = container.stage.stageHeight / 2 - 50;
			textField.border = true;
			textField.borderColor = 0;
			textField.selectable = false;
			textField.width = 150;
			textField.height = 40
			button.addChild(textField);
			button.addEventListener(MouseEvent.CLICK,onButtonClick);
		}
		
		protected function onButtonClick(event:MouseEvent):void
		{
			dispatcher.dispatchEvent(new GameEvent(GameEvent.RESTART));
		}
		
		override public function enterState():void {
			running = false;
			container.stage.addChild(button);
			//container.stage.dispatchEvent(Event.ENTER_FRAME,onEnterFrame);
			//container.stage.dispatchEvent(KeyboardEvent.KEY_DOWN,onSpacePressed);
		}
		
		override public function exitState():void {
			running = true;
			container.stage.removeChild(button);
		}
	}
}