package events
{
	import flash.events.Event;
	
	public class GameEvent extends Event
	{
		public static const PLAYER_DEATH:String = "playerDeath";
		public static const RESTART:String = "restart";
		
		public function GameEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
		}
	}
}